package com.nlmk.dezhemesov.taskmanager.service;

import com.nlmk.dezhemesov.taskmanager.entity.Task;
import com.nlmk.dezhemesov.taskmanager.repository.TaskRepository;

import java.util.List;

/**
 * Служба задач
 */
public class TaskService {

    /**
     * Репозиторий задач
     */
    private final TaskRepository taskRepository;

    /**
     * Конструктор
     *
     * @param taskRepository ссылка на репозиторий задач
     */
    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    /**
     * Создание задачи и помещение её в репозиторий
     *
     * @param name имя задачи
     * @return созданная задача
     */
    public Task create(String name) {
        return taskRepository.create(name);
    }

    /**
     * Очистка репозитория задач
     */
    public void clear() {
        taskRepository.clear();
    }

    /**
     * Получение списка задач
     *
     * @return список задач в репозитории
     */
    public List<Task> findAll() {
        return taskRepository.findAll();
    }

    /**
     * Поиск задачи по индексу в репозитории
     *
     * @param index индекс задачи в репозитории
     * @return найденная задача либо null, если не найдена
     */
    public Task findByIndex(int index) {
        if (index < 0 || index > taskRepository.size())
            return null;
        return taskRepository.findByIndex(index);
    }

    /**
     * Поиск задачи по имени
     *
     * @param name имя задачи
     * @return найденная задача либо null, если не найдена
     */
    public Task findByName(String name) {
        if (name == null || name.equals(""))
            return null;
        return taskRepository.findByName(name);
    }

    /**
     * Поиск задачи по идентификатору
     *
     * @param id идентификатор задачи
     * @return найденная задача либо null, если не найдена
     */
    public Task findById(Long id) {
        if (id == null)
            return null;
        return taskRepository.findById(id);
    }

}
