package com.nlmk.dezhemesov.taskmanager.controller;

import com.nlmk.dezhemesov.taskmanager.entity.Project;
import com.nlmk.dezhemesov.taskmanager.service.ProjectService;

/**
 * Контроллер проектов
 */
public class ProjectController extends AbstractController {

    /**
     * Хранилище проектов
     */
    private final ProjectService projectService;

    /**
     * Конструктор
     *
     * @param projectService служба проектов
     */
    public ProjectController(ProjectService projectService) {
        this.projectService = projectService;
    }

    /**
     * Просмотр проекта по индексу
     *
     * @return код возврата
     */
    public int viewByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Project project = projectService.findByIndex(index - 1);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр проекта по идентификатору
     *
     * @return код возврата
     */
    public int viewById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Project project = projectService.findById(id);
        viewProject(project);
        return 0;
    }

    /**
     * Просмотр проекта по имени
     *
     * @return код возврата
     */
    public int viewProjectByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Project project = projectService.findByName(name);
        viewProject(project);
        return 0;
    }

    /**
     * Редкатирование проекта по индексу
     *
     * @return код возврата
     */
    public int editProjectByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Project project = projectService.findByIndex(index - 1);
        editProject(project);
        return 0;
    }

    /**
     * Редактирование проекта по идентификатору
     *
     * @return код возврата
     */
    public int editProjectById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Project project = projectService.findById(id);
        editProject(project);
        return 0;
    }

    /**
     * Редактирование проекта по имени
     *
     * @return код возврата
     */
    public int editProjectByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Project project = projectService.findByName(name);
        editProject(project);
        return 0;
    }

    /**
     * Удаление проекта по индексу
     *
     * @return код возврата
     */
    public int removeProjectByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Project project = projectService.findByIndex(index - 1);
        removeProject(project);
        return 0;
    }

    /**
     * Удаление проекта по идентификатору
     *
     * @return код возврата
     */
    public int removeProjectById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Project project = projectService.findById(id);
        removeProject(project);
        return 0;
    }

    /**
     * Удаление проекта по имени
     *
     * @return код возврата
     */
    public int removeProjectByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Project project = projectService.findByName(name);
        removeProject(project);
        return 0;
    }

    /**
     * Просмотр атрибутов проекта
     *
     * @param project проект
     * @return проект
     */
    private Project viewProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[VIEW PROJECT]");
        System.out.println("ID: " + project.getId());
        System.out.println("Name: " + project.getName());
        System.out.println("Description: " + project.getDescription());
        System.out.println("[OK]");
        return project;
    }

    /**
     * Редактирование атрибутов проекта
     *
     * @param project проект
     * @return проект
     */
    private Project editProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[EDIT PROJECT]");
        System.out.print("Enter name: ");
        String name = readString();
        project.setName(name);
        System.out.print("Enter description: ");
        String description = readString();
        project.setDescription(description);
        System.out.println("[OK]");
        return project;
    }

    /**
     * Удаление проекта из хранилища
     *
     * @param project проект
     * @return проект
     */
    private Project removeProject(final Project project) {
        if (project == null)
            return null;
        System.out.println("[REMOVE PROJECT]");
        if (projectService.findAll().remove(project))
            System.out.println("[OK]");
        else
            System.out.println("SOMETHING WRONG");
        return project;
    }

    /**
     * Добавление проекта в хранилище
     *
     * @return код возврата
     */
    public int createProject() {
        System.out.println("[CREATE PROJECT]");
        System.out.print("Enter project name: ");
        final String name = readString();
        projectService.create(name);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Вывод списка проектов в хранилище
     *
     * @return код возврата
     */
    public int listProject() {
        System.out.println("[LIST PROJECT]");
        int i = 1;
        for (Project project : projectService.findAll())
            System.out.println("[" + (i++) + "]: " + project);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление всех проектов из хранилища
     *
     * @return код возврата
     */
    public int clearProject() {
        System.out.println("[CLEAR PROJECT]");
        projectService.clear();
        System.out.println("[OK]");
        return 0;
    }

}
