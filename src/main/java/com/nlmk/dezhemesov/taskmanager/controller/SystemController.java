package com.nlmk.dezhemesov.taskmanager.controller;

/**
 * Контроллер системных команд
 */
public class SystemController extends AbstractController {

    /**
     * Вывод строки-приветствия
     */
    public static void displayWelcome() {
        System.out.println("*** WELCOME TO THE TASK MANAGER ***");
    }

    /**
     * Сведения о программе
     */
    public int displayAbout() {
        System.out.println("Author: Serge Dezhemesov");
        System.out.println("        dserg1972@gmail.com");
        return 0;
    }

    /**
     * Сведенеия о версии
     */
    public int displayVersion() {
        System.out.println("Version: 1.0.0");
        return 0;
    }

    /**
     * Перечень команд
     */
    public int displayHelp() {
        System.out.println("Usage: java -jar taskmanager.jar [command]");
        System.out.println("Commands:");
        System.out.println("  about   - display developer info");
        System.out.println("  help    - display usage");
        System.out.println("  version - display version info");
        System.out.println("  exit    - terminate program");
        System.out.println();
        System.out.println("  project-create - create new project");
        System.out.println("  project-list   - display list of projects");
        System.out.println("  project-clear  - remove all projects");
        System.out.println();
        System.out.println("  task-create - create new task");
        System.out.println("  task-list   - display list of tasks");
        System.out.println("  task-clear  - remove all task");
        System.out.println();
        System.out.println("  project-view-by-index - view project properties by index");
        System.out.println("  project-view-by-name  - view project properties by name");
        System.out.println("  project-view-by-id    - view project properties by identifier");
        System.out.println();
        System.out.println("  project-edit-by-index - change project properties by index");
        System.out.println("  project-edit-by-name  - change project properties by name");
        System.out.println("  project-edit-by-id    - change project properties by identifier");
        System.out.println();
        System.out.println("  project-remove-by-index - remove project by index");
        System.out.println("  project-remove-by-name  - remove project by name");
        System.out.println("  project-remove-by-id    - remove project by identifier");
        System.out.println();
        System.out.println("  task-view-by-index - view task properties by index");
        System.out.println("  task-view-by-name  - view task properties by name");
        System.out.println("  task-view-by-id    - view task properties by identifier");
        System.out.println();
        System.out.println("  task-edit-by-index - change task properties by index");
        System.out.println("  task-edit-by-name  - change task properties by name");
        System.out.println("  task-edit-by-id    - change task properties by identifier");
        System.out.println();
        System.out.println("  task-remove-by-index - remove task by index");
        System.out.println("  task-remove-by-name  - remove task by name");
        System.out.println("  task-remove-by-id    - remove task by identifier");
        return 0;
    }

    /**
     * Сообщение об ошибочной команде
     */
    public int displayError() {
        System.out.println("Unknown command");
        return -1;
    }

    /**
     * Обработка команды выхода из программы
     *
     * @return результат вызова
     */
    public int exit() {
        System.out.println("Terminating program ...");
        System.exit(0);
        return 0;
    }

}
