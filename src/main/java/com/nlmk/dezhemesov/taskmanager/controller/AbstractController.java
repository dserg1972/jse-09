package com.nlmk.dezhemesov.taskmanager.controller;

import java.util.Scanner;

/**
 * Родительский класс контроллеров с общей логикой
 */
public class AbstractController {

    /**
     * Обработчик системного ввода
     */
    protected static final Scanner scanner = new Scanner(System.in);

    /**
     * Чтение команды из системного ввода
     *
     * @return команда
     */
    public static String readString() {
        String command;
        command = scanner.nextLine();
        return command;
    }

    /**
     * Чтение целочисленного значения из системного ввода
     *
     * @return введённая строка, интерпретированная как целочисленное значение
     */
    public static Integer readInteger() {
        Integer value = null;
        try {
            value = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException ex) {
            System.out.println("Can't parse integer value");
        }
        return value;
    }

    /**
     * Чтение Long значения из системного ввода
     *
     * @return введённая строка, интерпретированная как Long значение
     */
    public static Long readLong() {
        Long value = null;
        try {
            value = Long.parseLong(scanner.nextLine());
        } catch (NumberFormatException ex) {
            System.out.println("Can't parse Long value");
        }
        return value;
    }

}
