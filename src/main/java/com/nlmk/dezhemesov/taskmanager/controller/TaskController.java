package com.nlmk.dezhemesov.taskmanager.controller;

import com.nlmk.dezhemesov.taskmanager.entity.Task;
import com.nlmk.dezhemesov.taskmanager.service.TaskService;

/**
 * Контроллер задач
 */
public class TaskController extends AbstractController {

    /**
     * Хранилище задач
     */
    private final TaskService taskService;

    /**
     * Конструктор
     *
     * @param taskService служба задач
     */
    public TaskController(TaskService taskService) {
        this.taskService = taskService;
    }

    /**
     * Просмотр задачи по индексу
     *
     * @return код возврата
     */
    public int viewTaskByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Task task = taskService.findByIndex(index - 1);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр задачи по идентификатору
     *
     * @return код возврата
     */
    public int viewTaskById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Task task = taskService.findById(id);
        viewTask(task);
        return 0;
    }

    /**
     * Просмотр задачи по имени
     *
     * @return код возврата
     */
    public int viewTaskByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Task task = taskService.findByName(name);
        viewTask(task);
        return 0;
    }

    /**
     * Редкатирование задачи по индексу
     *
     * @return код возврата
     */
    public int editTaskByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Task task = taskService.findByIndex(index - 1);
        editTask(task);
        return 0;
    }

    /**
     * Редактирование задачи по идентификатору
     *
     * @return код возврата
     */
    public int editTaskById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Task task = taskService.findById(id);
        editTask(task);
        return 0;
    }

    /**
     * Редактирование задачи по имени
     *
     * @return код возврата
     */
    public int editTaskByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Task task = taskService.findByName(name);
        editTask(task);
        return 0;
    }

    /**
     * Удаление задачи по индексу
     *
     * @return код возврата
     */
    public int removeTaskByIndex() {
        System.out.print("Enter index: ");
        int index = readInteger();
        Task task = taskService.findByIndex(index - 1);
        removeTask(task);
        return 0;
    }

    /**
     * Удаление задачи по идентификатору
     *
     * @return код возврата
     */
    public int removeTaskById() {
        System.out.print("Enter id: ");
        Long id = readLong();
        Task task = taskService.findById(id);
        removeTask(task);
        return 0;
    }

    /**
     * Удаление задачи по имени
     *
     * @return код возврата
     */
    public int removeTaskByName() {
        System.out.print("Enter name: ");
        String name = readString();
        Task task = taskService.findByName(name);
        removeTask(task);
        return 0;
    }


    /**
     * Просмотр атрибутов задачи
     *
     * @param task задача
     * @return задача
     */
    private Task viewTask(final Task task) {
        if (task == null)
            return null;
        System.out.println("[VIEW TASK]");
        System.out.println("ID: " + task.getId());
        System.out.println("Name: " + task.getName());
        System.out.println("Description: " + task.getDescription());
        System.out.println("[OK]");
        return task;
    }

    /**
     * Редактирование атрибутов задачи
     *
     * @param task задача
     * @return задача
     */
    private Task editTask(final Task task) {
        if (task == null)
            return null;
        System.out.println("[EDIT TASK]");
        System.out.print("Enter name: ");
        String name = readString();
        task.setName(name);
        System.out.print("Enter description: ");
        String description = readString();
        task.setDescription(description);
        System.out.println("[OK]");
        return task;
    }

    /**
     * Удаление задачи из хранилища
     *
     * @param task задача
     * @return задача
     */
    private Task removeTask(final Task task) {
        if (task == null)
            return null;
        System.out.println("[REMOVE TASK]");
        if (taskService.findAll().remove(task))
            System.out.println("[OK]");
        else
            System.out.println("SOMETHING WRONG");
        return task;
    }

    /**
     * Добавление задачи в хранилище
     *
     * @return код возврата
     */
    public int createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter task name: ");
        final String name = readString();
        taskService.create(name);

        System.out.println("[OK]");
        return 0;
    }

    /**
     * Вывод списка задач в хранилище
     *
     * @return код возврата
     */
    public int listTask() {
        System.out.println("[LIST TASK]");
        int i = 1;
        for (Task task : taskService.findAll())
            System.out.println("[" + (i++) + "]: " + task);
        System.out.println("[OK]");
        return 0;
    }

    /**
     * Удаление всех задач из хранилища
     *
     * @return код возврата
     */
    public int clearTask() {
        System.out.println("[CLEAR TASK]");
        taskService.clear();
        System.out.println("[OK]");
        return 0;
    }

}
