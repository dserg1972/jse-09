package com.nlmk.dezhemesov.taskmanager.repository;

import com.nlmk.dezhemesov.taskmanager.entity.Task;

import java.util.ArrayList;
import java.util.List;

/**
 * Хранилище задач
 */
public class TaskRepository {

    /**
     * Список задач
     */
    private List<Task> tasks = new ArrayList<>();

    {
        create("TASK1", "Описание задачи 1");
        create("TASK2");
    }

    /**
     * Создание задачи c именем и добавление в хранилище
     *
     * @param name имя задачи
     * @return созданная задача
     */
    public Task create(final String name) {
        final Task task = new Task(name);
        tasks.add(task);
        return task;
    }

    /**
     * Создание задачи с именем и описанием и добавление в хранилище
     *
     * @param name        имя задачи
     * @param description описание задачи
     * @return созданная задача
     */
    public Task create(final String name, final String description) {
        final Task task = new Task(name, description);
        tasks.add(task);
        return task;
    }

    /**
     * Очистка хранилища
     */
    public void clear() {
        tasks.clear();
    }

    /**
     * Список задач
     *
     * @return список задач
     */
    public List<Task> findAll() {
        return tasks;
    }

    /**
     * Поиск задачи в хранилище по индексу
     *
     * @param index индекс задачи в хранилище
     * @return найденная задача либо null, если задача не найдена либо неправильный индекс
     */
    public Task findByIndex(int index) {
        return tasks.get(index);
    }

    /**
     * Поиск задачи в хранилище по имени
     *
     * @param name имя задачи
     * @return найденная задача либо null, если задача не найдена
     */
    public Task findByName(String name) {
        for (Task task : tasks)
            if (task.getName().equals(name))
                return task;
        return null;
    }

    /**
     * Поиск задачи в хранилище по идентификатору
     *
     * @param id идентификатор задачи
     * @return найденная задача либо null, если задача не найдена
     */
    public Task findById(Long id) {
        for (Task task : tasks)
            if (task.getId().equals(id))
                return task;
        return null;
    }

    /**
     * Получение размера хранилища
     *
     * @return Размер хранилища
     */
    public int size() {
        return tasks.size();
    }

    public static void main(String[] args) {
        TaskRepository dao = new TaskRepository();
        Task task = dao.findByName("TASK2");
        System.out.println(task);
    }

}
